import requests
from config import url_host, url_orgs, url_ctrs, url_acrs
from ddauth import get_headers, get_ddauth_token_by_login


class DiadocAPI(object):

    def __init__(self):
        self.ddauth_token = get_ddauth_token_by_login()
        self.headers = get_headers(ddauth_token=self.ddauth_token)

    def request(self, method='GET', url=url_host, **kwargs):
        response = requests.request(method=method, url=url, headers=self.headers, **kwargs)
        if response.status_code != 200: return None
        else: return response.json()

    def get_my_organizations(self):
        return self.request(url=url_orgs)

    def get_counteragents(self, org, after_index):
        params = {"myOrgId": org,
                  "afterIndexKey": after_index,
                  "counteragentStatus": "InvitesMe"}
        return self.request(url=url_ctrs, params=params)

    def get_acquire_counteragent(self, org, data):
        params = {"myOrgId": org}
        return self.request(method='POST', url=url_acrs, params=params, data=data)


if __name__ == '__main__':
    pass
