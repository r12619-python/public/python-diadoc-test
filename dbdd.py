import json
import datetime
from dbcreate import DBCreate


class DiadocDB(DBCreate):

    def __init__(self, conn_str, reset=False):
        super().__init__(conn_str, reset)
        self.tbl_organizations_register_insert()
        self.tbl_organizations_accepted_insert()

    def tbl_organizations_register_insert(self):
        sql = """
              SET ANSI_WARNINGS  OFF
              IF NOT EXISTS (SELECT TOP 1 * FROM [diadoc_test].[dbo].[tbl_organizations_register])
              INSERT INTO [dbo].[tbl_organizations_register]
                          ([Inn_our], [Inn], [Name], [Summ])
                   VALUES ('111111111111', '3333333333333', 'А', 100)
                        , ('222222222222', '3333333333333', 'А', 200)
                        , ('111111111111', '4444444444444', 'Б', 300)
                        , ('222222222222', '5555555555555', 'В', 100)
              SET ANSI_WARNINGS ON
              """
        self.execute_commit_sql(sql)

    def tbl_organizations_accepted_insert(self):
        sql = """
              SET ANSI_WARNINGS  OFF
              IF NOT EXISTS (SELECT TOP 1 * FROM [diadoc_test].[dbo].[tbl_organizations_accepted])
              INSERT INTO [dbo].[tbl_organizations_accepted]
                          ([Inn_our], [Inn], [accepted])
                   VALUES ('111111111111', '3333333333333', 1)
                        , ('222222222222', '5555555555555', 1)
              SET ANSI_WARNINGS ON
              """
        self.execute_commit_sql(sql)

    def get_percentage_inn(self):
        sql = """
              SELECT [Inn_our]
                   --, [sum_our]
                   --, [sum_our_acptd]
                   , [prc_sum]
                   --, [prc_sum_our]
                   --, [cnt_our]
                   --, [cnt_our_acptd]
                   , [prc_cnt]
                   --, [prc_cnt_our]
                FROM [dbo].[view_percentage_inn] 
              """
        return self.execute_sql(sql).to_dict()

    def tbl_organizations_insert(self, diadoc):
        """ обновить реестр организаций и подразделений в базе данных """
        self.execute_commit_sql('truncate table diadoc_test.dbo.tbl_organizations')
        self.execute_commit_sql('truncate table diadoc_test.dbo.tbl_departments')
        r1 = diadoc.get_my_organizations()
        for i in range(len(r1['Organizations'])):
            if not r1['Organizations'][i]['IsTest']:
                sql = 'INSERT INTO diadoc_test.dbo.tbl_organizations VALUES(' + "'" + \
                      r1['Organizations'][i]['OrgIdGuid'] + "', '" + \
                      r1['Organizations'][i]['OrgId'] + "', '" + \
                      r1['Organizations'][i]['Inn'] + "', '" + \
                      r1['Organizations'][i]['Kpp'] + "', '" + \
                      r1['Organizations'][i]['FullName'] + "', '" + \
                      r1['Organizations'][i]['ShortName'] + "', '" + \
                      r1['Organizations'][i]['Boxes'][0]['BoxId'] + "', '" + \
                      r1['Organizations'][i]['Boxes'][0]['BoxIdGuid'] + "', '" + \
                      r1['Organizations'][i]['Boxes'][0]['Title'] + "', '" + \
                      r1['Organizations'][i]['Boxes'][0]['InvoiceFormatVersion'] + "', '" + \
                      r1['Organizations'][i]['Ogrn'] + "', '" + \
                      r1['Organizations'][i]['FnsParticipantId'] + "')"
                self.execute_commit_sql(sql)
            for j in range(len(r1['Organizations'][i]['Departments'])):
                if not r1['Organizations'][i]['Departments'][j]['IsDisabled']:
                    buff = r1['Organizations'][i]['Departments'][j]
                    sql = 'INSERT INTO diadoc_test.dbo.tbl_departments VALUES(' + "'" + \
                          buff['DepartmentId'] + "', '" + \
                          buff['ParentDepartmentId'] + "', '" + \
                          buff['Name'] + "', '" + \
                          buff['Abbreviation'] + "', '" + \
                          r1['Organizations'][i]['OrgId'] + "')"
                    self.execute_commit_sql(sql)

    def tbl_invitations_insert(self, diadoc):
        """ обновить реестр приглашений """
        self.execute_commit_sql('truncate table diadoc_test.dbo.tbl_invitations')
        orgs = self.cursor.execute('select distinct OrgId from dbo.tbl_organizations').fetchall()
        orgs_list = list(orgs[x][0] for x in range(len(orgs)))
        for org in orgs_list:
            after_index = -1
            counter = 0
            eol = False
            while not eol:
                r1 = diadoc.get_counteragents(org, after_index)
                if r1.text[:8] != 'Доступ з':
                    r1 = r1.json()
                    for i in range(len(r1['Counteragents'])):
                        buff = r1['Counteragents'][i]['Organization']
                        sql = 'INSERT INTO diadoc_test.dbo.tbl_invitations VALUES(' + "'" + \
                                    buff['ShortName'] + "', '" + \
                                    org + "', '" + \
                                    buff['Inn'] + "', '" + \
                                    buff['OrgId'] + "')"
                        self.execute_commit_sql(sql)
                    if len(r1['Counteragents']) < 99:
                        eol = True
                    else:
                        # максимум 100 записей, запоминаем индекс, передаем в сл. запрос
                        after_index = r1['Counteragents'][len(r1['Counteragents']) - 1]['IndexKey']
                else:
                    eol = True

    def tbl_invitations_accepted_insert(self, diadoc):
        """ забираем с сервера список ИНН, OrgID, очищенный от неизвестных лиц,
            и принимаем приглашения """
        a = self.cursor.execute('select distinct OrId_our, OrgID, ИНН from dbo.view_Приглашения_от_моих_КА').fetchall()
        ka_list = list([a[x][0], a[x][1], a[x][2]] for x in range(len(a)))
        for i in range(len(ka_list)):
            r1 = diadoc.get_acquire_counteragent(ka_list[i][0], json.dumps({"OrgId": ka_list[i][1]}))
            # если приглашение требует подписи - оно не примется
            if r1.text[0:6] == 'Cannot':
                pass
            else:
                # если приглашение "простое" - оно принято, вносим в таблицу на сервере
                sql = 'INSERT INTO dbo.tbl_invitations_accepted VALUES (' + "'" + \
                            ka_list[i][1] + "', '" + \
                            datetime.datetime.now().strftime('%Y-%m-%d') + "', '" + \
                            ka_list[i][2] + "', '" + \
                            ka_list[i][1] + "')"
                self.execute_commit_sql(sql)


if __name__ == "__main__":
    pass

