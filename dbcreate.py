from dbsql import MsSqlDB


class DBCreate(MsSqlDB):

    def __init__(self, conn_str, reset=False):
        super().__init__(conn_str)
        self.execute_sql('SET DATEFORMAT ymd')
        if reset:
            self.tbl_organizations_drop()
            self.tbl_departments_drop()
            self.tbl_invitations_drop()
            self.tbl_invitations_accepted_drop()
            self.tbl_organizations_register_drop()
            self.tbl_organizations_accepted_drop()
        self.tbl_organizations_create()
        self.tbl_departments_create()
        self.tbl_invitations_create()
        self.tbl_invitations_accepted_create()
        self.tbl_organizations_register_create()
        self.tbl_organizations_accepted_create()

        self.view_percentage_inn_drop()
        self.view_percentage_inn_create()

    def drop_object(self, name, type='U'):
        object = {'U': 'TABLE', 'V': 'VIEW'}
        sql = f"""
              IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{name}]') AND type in (N'{type}'))
              DROP {object[type]} [dbo].[{name}]
              """
        if name and type in object.keys(): self.execute_sql(sql)

    def view_percentage_inn_drop(self):
        self.drop_object('view_percentage_inn', 'V')

    def drop_table(self, name):
        self.drop_object(name)

    def tbl_organizations_drop(self):
        self.drop_table('tbl_organizations')

    def tbl_departments_drop(self):
        self.drop_table('tbl_departments')

    def tbl_invitations_drop(self):
        self.drop_table('tbl_invitations')

    def tbl_invitations_accepted_drop(self):
        self.drop_table('tbl_invitations_accepted')

    def tbl_organizations_register_drop(self):
        self.drop_table('tbl_organizations_register')

    def tbl_organizations_accepted_drop(self):
        self.drop_table('tbl_organizations_accepted')

    def tbl_organizations_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_organizations]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_organizations](
                  [OrgGUID] [char](36) NULL,
                  [OrgID] [char](36) NULL,
                  [Inn] [char](12) NULL,
                  [Kpp] [char](9) NULL,
                  [Full_name] [nvarchar](max) NULL,
                  [Short_name] [nvarchar](max) NULL,
                  [BoxID] [varchar](100) NULL,
                  [BoxGUID] [char](36) NULL,
                  [Box_title] [nvarchar](max) NULL,
                  [Invoice_format_ver] [char](20) NULL,
                  [Ogrn] [char](15) NULL,
                  [FNS_participant] [char](50) NULL
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def tbl_departments_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_departments]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_departments](
                  [DepartmentID] [char](36) NULL,
                  [Parent_dept_ID] [char](36) NULL,
                  [Dept_name] [nvarchar](max) NULL,
                  [Dept_abbr] [nvarchar](50) NULL,
                  [OrgID] [char](36) NULL
              ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
              """
        self.execute_sql(sql)

    def tbl_invitations_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_invitations]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_invitations](
                  [Short_name] [nvarchar](255) NULL,
                  [OrgID_our] [char](36) NULL,
                  [Inn] [char](12) NULL,
                  [OrgID] [char](36) NULL
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def tbl_invitations_accepted_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_invitations_accepted]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_invitations_accepted](
                  [OrgId] [char](36) NULL,
                  [Record_date] [date] NULL,
                  [Inn] [char](12) NULL,
                  [OrgId_our] [char](36) NULL
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def tbl_organizations_register_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_organizations_register]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_organizations_register](
                  [Inn_our] [char](12) NULL,
                  [Inn] [char](12) NULL,
                  [Name] [nvarchar](max) NULL,
                  [Summ] [real] NULL,
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def tbl_organizations_accepted_create(self):
        sql = """
              IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_organizations_accepted]') AND type in (N'U'))
              CREATE TABLE [dbo].[tbl_organizations_accepted](
                  [Inn_our] [char](12) NULL,
                  [Inn] [char](12) NULL,
                  [accepted] [bit] NULL,
              ) ON [PRIMARY]
              """
        self.execute_sql(sql)

    def view_percentage_inn_create(self):
        sql = """
              --IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[view_percentage_inn]') AND type in (N'V'))
              CREATE VIEW [dbo].[view_percentage_inn]
              AS
              WITH CN AS (
                 SELECT ka.[Inn_our], ka.[Inn] AS [Inn_], ka.[Name], CAST(ka.[Summ] AS real) AS [Summ]
                      , inv.[Inn], ISNULL(inv.[accepted],0) AS [accepted]
                   FROM [dbo].[tbl_organizations_register] AS ka
              LEFT JOIN [dbo].[tbl_organizations_accepted] AS inv ON inv.[Inn] = ka.[Inn]
                                                                 --AND inv.[Inn_our] = ka.[Inn_our]
              ),
              SC AS (
              SELECT [Inn_our], [Inn_], [Name], [Summ], [Inn], [accepted]
                   , SUM([Summ]) OVER() AS [sum_all]
                   , SUM([Summ]) OVER(PARTITION BY [accepted]) AS [sum_acptd]
                   , SUM([Summ]) OVER(PARTITION BY [Inn_our]) AS [sum_our]
                   , SUM([Summ]) OVER(PARTITION BY [Inn_our], [accepted]) AS [sum_our_acptd]
                   , COUNT([Summ]) OVER() AS [cnt_all]
                   , COUNT([Summ]) OVER(PARTITION BY [accepted]) AS [cnt_acptd]
                   , COUNT([Summ]) OVER(PARTITION BY [Inn_our]) AS [cnt_our]
                   , COUNT([Summ]) OVER(PARTITION BY [Inn_our], [accepted]) AS [cnt_our_acptd]
                FROM CN
              )
              SELECT [Inn_our]
                   --, [sum_our]
                   --, [sum_our_acptd]
                   , ROUND(100*[sum_our_acptd]/[sum_all],2) AS [prc_sum]
                   --, ROUND(100*[sum_our_acptd]/[sum_our],2) AS [prc_sum_our]
                   --, [cnt_our]
                   --, [cnt_our_acptd]
                   , ROUND(100*[cnt_our_acptd]/[cnt_all],2) AS [prc_cnt]
                   --, ROUND(100*[cnt_our_acptd]/[cnt_our],2) AS [prc_cnt_our]
                FROM SC
               WHERE [accepted] <> 0
              UNION
              SELECT '-----all-----' AS [Inn_our] -- '----------all----------'
                   --, [sum_all]
                   --, [sum_acptd]
                   , ROUND(100*[sum_acptd]/[sum_all],2) AS [prc_sum]
                   --, NULL AS [prc_sum_our]
                   --, [cnt_all]
                   --, [cnt_acptd]
                   , ROUND(100*[cnt_acptd]/[cnt_all],2) AS [prc_cnt]
                   --, NULL AS [prc_cnt_our]
                FROM SC
               WHERE [accepted] <> 0
              """
        self.execute_sql(sql)


if __name__ == "__main__":
    pass

