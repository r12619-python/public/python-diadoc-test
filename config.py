sql_connection_string = 'DRIVER={SQL Server};SERVER=localhost\SQLEXPRESS;DATABASE=diadoc_test;TRUSTED_CONNECTION=yes'
ddauth_api_client_id = 'testClient-8ee1638deae84c86b8e2069955c2825a'
# ddauth_token = '3IU0iPhuhHPZ6lrlumGz4pICEedhQ1XmlMN1Pk8z0DJ51MXkcTi6Q3CODCC4xTMsjPFfhK6XM4kCJ4JJ42hlD499/Ui5WSq6lrPwcdp4IIKswVUwyE0ZiwhlpeOwRjNrvUX1yPrxr0dY8a0w8ePsc1DG8HAlZce8a0hZiWylMqu23d/vfzRFuA=='
# Authorization = f'DiadocAuth ddauth_api_client_id={ddauth_api_client_id},ddauth_token={ddauth_token}'
login = 'user@skbkontur.ru'
password = 'qwerty'
url_host = 'https://diadoc-api.kontur.ru'
url_auth = f'{url_host}/V3/Authenticate'
url_orgs = f'{url_host}/GetMyOrganizations'
url_ctrs = f'{url_host}/V2/GetCounteragents'
url_acrs = f'{url_host}/V2/AcquireCounteragent'



""" ----------------------------------------------------------------
    https://diadoc-sdk.readthedocs.io/ru/latest/Authorization.html
    https://diadoc-sdk.readthedocs.io/ru/latest/howto/example_authorization.html """
""" Чтобы получить авторизационный токен по сертификату, 
    клиент должен расшифровать тело ответа команды Authenticate при помощи закрытого ключа, 
    соответствующего пользовательскому сертификату. 
    Полученный массив байтов нужно закодировать в Base64 - строку. """
""" ----------------------------------------------------------------
-- получить авторизационный токен, используя команду Authenticate
--
POST https://diadoc-api.kontur.ru/Authenticate HTTP/1.1
Host: diadoc-api.kontur.ru
Authorization: DiadocAuth ddauth_api_client_id=testClient-8ee1638deae84c86b8e2069955c2825a
Content-Length: 1252
Connection: Keep-Alive

<Двоичное DER-представление X.509-сертификата пользователя>
----------------------------------------------------------------
-- ответ

HTTP/1.1 200 OK
Content-Length: 598

<Двоичное DER-представление зашифрованного авторизационного токена> 
---------------------------------------------------------------- """

""" ----------------------------------------------------------------
-- получить авторизационный токен с помощью логина и пароля
--
POST /Authenticate?login=user@skbkontur.ru&password=qwerty HTTP/1.1
Host: diadoc-api.kontur.ru
Authorization: DiadocAuth ddauth_api_client_id=testClient-8ee1638deae84c86b8e2069955c2825a
Content-Length: 1252
Connection: Keep-Alive
----------------------------------------------------------------
-- ответ

HTTP/1.1 200 OK
Content-Length: 598

<Авторизационный токен>
---------------------------------------------------------------- """

""" ----------------------------------------------------------------
-- далее к каждому HTTP-запросу к Диадоку требуется добавлять HTTP-заголовок 
   Authorization с параметрами ddauth_api_client_id= и ddauth_token=.
----------------------------------------------------------------
-- пример

POST https://diadoc-api.kontur.ru/GetMyOrganizations HTTP/1.1
Host: diadoc-api.kontur.ru
Authorization: DiadocAuth ddauth_api_client_id=testClient-8ee1638deae84c86b8e2069955c2825a,ddauth_token=3IU0iPhuhHPZ6lrlumGz4pICEedhQ1XmlMN1Pk8z0DJ51MXkcTi6Q3CODCC4xTMsjPFfhK6XM4kCJ4JJ42hlD499/Ui5WSq6lrPwcdp4IIKswVUwyE0ZiwhlpeOwRjNrvUX1yPrxr0dY8a0w8ePsc1DG8HAlZce8a0hZiWylMqu23d/vfzRFuA==
----------------------------------------------------------------
-- пример

POST /GetMyOrganizations HTTP/1.1
Host: diadoc-api.kontur.ru
Authorization: DiadocAuth ddauth_api_client_id=testClient-8ee1638deae84c86b8e2069955c2825a,ddauth_token=3IU0iPhuhHPZ6lrlumGz4pICEedhQ1XmlMN1Pk8z0DJ51MXkcTi6Q3CODCC4xTMsjPFfhK6XM4kCJ4JJ42hlD499/Ui5WSq6lrPwcdp4IIKswVUwyE0ZiwhlpeOwRjNrvUX1yPrxr0dY8a0w8ePsc1DG8HAlZce8a0hZiWylMqu23d/vfzRFuA==
----------------------------------------------------------------
"""
