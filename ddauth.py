import requests
from config import url_auth, ddauth_api_client_id, login, password


def request(method, url, **kwargs):
    response = requests.request(method=method, url=url, **kwargs)
    if response.status_code != 200: return None
    else: return response.text

def get_headers(ddauth_api_client_id=ddauth_api_client_id, ddauth_token=None):
    """ HTTP-заголовки """
    Authorization = f'DiadocAuth ddauth_api_client_id={ddauth_api_client_id}'
    if ddauth_token: Authorization += f',ddauth_token={ddauth_token}'
    headers = {"Authorization": Authorization,
               "Content-Length": "1252",
               "Connection": "Keep-Alive"}
    return headers

def get_ddauth_token_by_login(login=login, password=password, ddauth_api_client_id=ddauth_api_client_id):
    """ получить авторизационный токен с помощью логина и пароля """
    params = {'login': login, 'password': password}
    headers = get_headers(ddauth_api_client_id=ddauth_api_client_id)
    return request(method='POST', url=url_auth, headers=headers, params=params)


def get_ddauth_token_by_client_id(ddauth_api_client_id=ddauth_api_client_id):
    """ получить авторизационный токен с помощью сертификата """
    headers = get_headers(ddauth_api_client_id=ddauth_api_client_id)
    return request(method='POST', url=url_auth, headers=headers)


if __name__ == '__main__':
    pass
