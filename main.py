from config import sql_connection_string
from diadoc_api import DiadocAPI
from dbdd import DiadocDB


db = DiadocDB(sql_connection_string, reset=False) # False # True # Пересоздать таблицы

def main():
    """ view, показывающий долю ИНН поставщиков, с которыми установлена связь в системе Контур.Диадок,
        по сумме и по количеству, в разрезе покупателей и в общем по всем покупателям """
    data = db.get_percentage_inn()
    for d in data:
        print(d)

def ddmain():
    """ Авторизация и получение данных из системы Диадок
        не задействовано, т.к. нет возможности протестировать """
    diadoc = DiadocAPI()
    # orgs = diadoc.get_my_organizations()
    db.tbl_organizations_insert(diadoc)
    # ctrs = diadoc.get_counteragents()
    db.tbl_invitations_insert(diadoc)
    # acrs = diadoc.get_acquire_counteragent()
    db.tbl_invitations_accepted_insert(diadoc)


if __name__ == '__main__':
    main()
